<?php

$json = file_get_contents('php://input');

$filename = 'main';

if($_SERVER['REQUEST_URI'] == '/fail')
    $filename = 'fail';
elseif($_SERVER['REQUEST_URI'] == '/success')
    $filename = 'success';
elseif($_SERVER['REQUEST_URI'] == '/notify')
    $filename = 'notify';
elseif($_SERVER['REQUEST_URI'] == '/interactive')
    $filename = 'interactive';

if($filename == 'notify')
    file_put_contents("{$filename}.json", $json, FILE_APPEND);
else
    file_put_contents("{$filename}.json", $json);
