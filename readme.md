# Каллбейкер для Saltage
**Просто переносит `JSON` из `POST` в файл **

**Типы файлов**

* fail.json - когда ошибки
* notify.json - когда зрен знает
* success.json - когда успешно зафетчился
* interactive.json - этапы фетчинга (смс автоизация, каптча, etc..)


**Пример interactive ответа**

```
{
  "data": {
    "login_id": 123,
    "customer_id": "MhCFgXcIceT8P9J61X56v-WAJ6xReVYdPp-1Wruo5a0",
    "stage": "interactive",
    "html": "<div id='interactive'><img src='https://docs.saltedge.com/images/saltedge_captcha.png' width='10' height='10' alt='some description' title='some description'></div>",
    "session_expires_at": "2015-10-08T08:47:11Z",
    "interactive_fields_names": ["image"]
  },
  "meta": {
    "version": "2",
    "time": "2015-10-08T07:47:11Z"
  }
}
```